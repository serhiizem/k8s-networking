package k8s.networking.back_end.module;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {

    @GetMapping
    public String getResponse() {
        return "Response from back end\n";
    }
}
